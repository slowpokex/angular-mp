const app = require('./server');

// Starting app point
app.listen(process.env.PORT || 8080);
